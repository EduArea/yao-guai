### 2016-07-06
 
Adafruit-ILI9340 support deleted, all the files are cleared. Tools was switched on the platformio project.
Added Adafruit SSD1306 support.

### 2016-07-07

SSD1306 works now. Check, why it works only in 5V
Sensors works now on INT2, INT3 (RX, TX)

### 2016-07-09

change display template, add interrupt for mode button

### 2016-07-10

templates. check why level so low
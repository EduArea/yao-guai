//
// Created by Stanislav Lakhtin on 17/07/16.
//

#ifndef YAO_GUAI_SETDATETIMEVIEW_H
#define YAO_GUAI_SETDATETIMEVIEW_H

#include "YaoGuai.h"
#include "DefaultView.h"
#include <RTClib.h>

enum SelectedValue { YEAR=0, MONTH=1, DAY=2, HOUR=3, MINUTE=4};
#define SELECTED_HEIGHT 16
#define UNSELECTED_HEIGHT 8

class SetDateTimeView: public DefaultView {
private:
    bool timeView;
    SelectedValue sValue;
    bool blink;
    bool increaseIt, decreaseIt;
    long lastUpdate;
    uint16_t year;
    uint8_t month, day, hour, minute;
    void syncDateValues(bool reset = false);
    void increaseDecreaseValue(int val);
public:
    SetDateTimeView(Adafruit_SSD1306 *display, RTC_DS1307 *rt, YaoGuai *device) : DefaultView(display, rt, device){
        this->timeView = false;
        this->lastUpdate = millis();
        this->syncDateValues(true);
        sValue = YEAR;
    }
    void RenderView(void);
    SelectedValue getSelected();
    void nextValue();
    void OnPlusButtonPress(void);
    void OnPlusButtonRelease(void);
    void OnMinusButtonPress(void);
    void OnMinusButtonRelease(void);
};


#endif //YAO_GUAI_SETDATETIMEVIEW_H

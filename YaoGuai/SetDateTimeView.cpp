//
// Created by Stanislav Lakhtin on 17/07/16.
//

#include "SetDateTimeView.h"

void SetDateTimeView::RenderView(void) {
    display->clearDisplay();
    if (!rtc->isrunning()) {
        //todo make ERROR STATE
        this->print("data&time error",0,0);
        return;
    }
    syncDateValues();
    if (timeView) {
        this->print(("setting time"), 0, 0);
        this->print(F("hour: "), 0, 16, (this->sValue == HOUR ? 2 : 1));
        display->print(hour);
        this->print(F("minute: "), 0, 16 + 8 * (this->sValue == MINUTE ? 1 : 2),
                    (this->sValue == MINUTE ? 2 : 1));
        display->print(minute);
    } else {
        this->print(F("setting date:"), 0, 0);
        this->print(F("year: "), 0, 16, (this->sValue == YEAR ? 2 : 1));
        display->print(year);
        this->print(F("month: "), 0, 16 + 8 * (this->sValue == YEAR ? 2 : 1), (this->sValue == MONTH ? 2 : 1));
        display->print(month);
        this->print(F("day: "), 0, 16 + 8 * (this->sValue == DAY ? 2 : 3), (this->sValue == DAY ? 2 : 1));
        display->print(day);
    }
}

void SetDateTimeView::syncDateValues(bool reset) {
  DateTime dt = rtc->now();
  year = this->sValue == YEAR && !reset ? year : dt.year();
  month = this->sValue == MONTH && !reset ? month : dt.month();
  day = this->sValue == DAY && !reset ? day : dt.day();
  hour = this->sValue == HOUR && !reset ? hour : dt.hour();
  minute = this->sValue == MINUTE && !reset ? minute : dt.minute();
  if (increaseIt) {
    increaseDecreaseValue(1);
  }
  if (decreaseIt) {
    increaseDecreaseValue(-1);
  }

}

void SetDateTimeView::increaseDecreaseValue(int val) {
  switch (sValue) {
    case YEAR:
      this->year += val;
      break;
    case MONTH:
      this->month += val;
      break;
    case DAY:
        this->day += val;
        break;
    case HOUR:
        this->hour += val;
        break;
    case MINUTE:
        this->minute += val;
        break;
  }
}

SelectedValue SetDateTimeView::getSelected() {
    return this->sValue;
}

void SetDateTimeView::nextValue() {
    switch (this->sValue) {
        default:
        case (YEAR):
            this->sValue = MONTH;
            break;
        case (MONTH):
            this->sValue = DAY;
            break;
        case (DAY):
            this->sValue = HOUR;
            break;
        case (HOUR):
            this->sValue = MINUTE;
            break;
        case (MINUTE):
            this->sValue = YEAR;
            break;
    }
    this->timeView = (sValue > DAY);
}

void SetDateTimeView::OnPlusButtonPress(void) {
    increaseIt = true;
}
void SetDateTimeView::OnPlusButtonRelease(void) {
    increaseIt = false;
}
void SetDateTimeView::OnMinusButtonPress(void) {
    decreaseIt = true;
}
void SetDateTimeView::OnMinusButtonRelease(void) {
    decreaseIt = false;
}

//
// Created by Stanislav Lakhtin on 12/07/16.
//

#include "ChannelDiagView.h"
#include "YaoGuai.h"

void ChannelDiagView::RenderView(int index, uint16_t collectedImpulses) {
    display->fillRect(112, 0, 16, 16, BLACK);
    this->print(((index == CHANNEL_A) ? "A" : "B"), 112, 0, 2, WHITE);
    display->fillRect(90, 16, 38, 32, BLACK);
    display->fillRect(0, 60, 128, 4, BLACK);
    long inSec = abs(millis() - lastChangeModeTime) / 1000;
    this->print(inSec, 90, 16);
    display->println(" sec");
    this->printLongAsFloat(device->sensitivity[index], 90, 24, 3 );
    this->print(device->quantity[index], 90, 32);
    this->print(collectedImpulses, 90, 40);
    if (collectedImpulses < 8)
        display->fillRect(0, 90, collectedImpulses * 16, 4, WHITE);
    else if (autoCompleteChannelDiag) {
        device->setState((device->getState() == INITIALIZE_CHANNEL_A ? INITIALIZE_CHANNEL_B : DEFAULT_STATE));
    }
}

void ChannelDiagView::DrawForm( void ) {
    this->print(F("channel : "), 0, 0);
    this->print(F("       delta : "), 0, 16);
    this->print(F("sensetivity. : "), 0, 24);
    this->print(F("    capacity : "), 0, 32);
    this->print(F("   impulses. : "), 0, 40);
    lastChangeModeTime = millis();
}

//
// Created by stanislav on 24.06.15.
//

#ifndef YAO_GUAI_YAOGUAI_H
#define YAO_GUAI_YAOGUAI_H


#define bit_get(p,m) ((p) & (m))
#define bit_set(p,m) ((p) |= (m))
#define bit_clear(p,m) ((p) &= ~(m))
#define bit_flip(p,m) ((p) ^= (m))
#define bit_write(c,p,m) (c ? bit_set(p,m) : bit_clear(p,m))
#define BIT(x) (0x01 << (x))

#define TRUE 1
#define FALSE 0

#include <Arduino.h>

#define CHANNEL_A 0
#define CHANNEL_B 1

#define SENSITIVITY 20 // СБМ-20 чувствительность составляет 0,016(81%)-0,02(по Пуассону 2/2^1-/2) имп/с -1 на мкР/ч -1 (по 137 Cs)
                          // показатель увеличен на 1000, чтобы избежать плавающую точку

#define TIMELINELENGTH 24 // длинна очереди по TIMELINECAPACITY
#define TIMELINECAPACITY 5 // в секундах. Общая длительность очереди подсчёта TIMELINELENGTH * TIMELINECAPACITY (в секундах)

#define KEY_MODE  B00000001
#define KEY_PLUS  B00000010
#define KEY_MINUS B00000100

#define SECOND 1000

#define COMMAND_SWITCHMODE  B00000001
#define COMMAND_SETDATETIME B00000010

enum YaoGuaiState {INITIALIZE_CHANNEL_A=1, INITIALIZE_CHANNEL_B=2, DEFAULT_STATE=3, CHANNEL_A_DISPLAY=4, CHANNEL_B_DISPLAY=5, ERROR=6, SET_TIME=7};

struct TimeLine {
    int      head;
    bool     overflow;
    long     lastCheck;
    uint16_t max;
    uint16_t count[TIMELINELENGTH];
};

class YaoGuai {
private:
    TimeLine channel[2];
    YaoGuaiState state;
    uint16_t year;
    uint8_t month, day, hour, minute;
public:
    YaoGuai();
    YaoGuaiState getState();
    int      lastValuesHead();
    void     setState(YaoGuaiState s);
    void     push(int index, int value);
    unsigned long getMRpH(int index);
    unsigned long sensitivity[2];
    int      quantity[2];
    uint16_t getSecondsInLine(int index);
    uint16_t getPulsesInLine(int index);
    void     (*changeViewEventHandler)(void);
protected:
    void     initTL(TimeLine* line);
    void     pushInTL(TimeLine *tl, uint16_t value);
    uint16_t getDelta(TimeLine *tl);
};

#endif //YAO_GUAI_YAOGUAI_H

//
// Created by Stanislav Lakhtin on 06/07/16.
//

#include <Arduino.h>
#include "YaoGuai.h"
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "ChannelDiagView.h"
#include "SetDateTimeView.h"
#include "Buttons.h"
#include <RTClib.h>

#define BUTTON_MINUS 5
#define BUTTON_PLUS 6
#define BUTTON_MODE 10

Adafruit_SSD1306 display(-1);
RTC_DS1307 RTC;
Buttons buttons;

YaoGuai *device;
ChannelDiagView *diagView;
DefaultView *defaultView;
SetDateTimeView *setDateTimeView;

volatile uint8_t collector[2];

//обработчик прерывания от сенсоров
void sensorsInterrupt(void) {
    uint8_t pinB = PINB;
    if ((pinB >> 5) & 1)
        collector[CHANNEL_A]++;
    if ((pinB >> 4) & 1)
        collector[CHANNEL_B]++;
}

//обработчик прерывания от кнопок
void keypadInterrupt(void) {
    buttons.processIt();
}

volatile uint16_t temporaryCollectedImpulses;

void buttonModePress() {
    Serial.println("Mode Button press");
}

void buttonPlusPress() {
    Serial.println("Plus Button press");
    switch (device->getState()) {
      case SET_TIME:
        setDateTimeView->OnPlusButtonPress();
        break;
    }
}

void buttonPlusRelease() {
    Serial.println("Plus Button release");
    switch (device->getState()) {
      case SET_TIME:
        setDateTimeView->OnPlusButtonRelease();
        break;
    }
}

void buttonMinusPress() {
    Serial.println("Plus Button press");
    switch (device->getState()) {
      case SET_TIME:
        setDateTimeView->OnMinusButtonPress();
        break;
    }
}

void buttonMinusRelease() {
    Serial.println("Plus Button release");
    switch (device->getState()) {
      case SET_TIME:
        setDateTimeView->OnMinusButtonRelease();
        break;
    }
}

void buttonModeRelease() {
    Serial.println("Mode Button release");
    switch (device->getState()) {
        case (SET_TIME):
            setDateTimeView->nextValue();
            if (setDateTimeView->getSelected() == YEAR)
                device->setState(DEFAULT_STATE);
            break;
        case (INITIALIZE_CHANNEL_A):
            display.clearDisplay();
            device->setState(INITIALIZE_CHANNEL_B);
            break;
        case (INITIALIZE_CHANNEL_B):
            display.clearDisplay();
            device->setState(DEFAULT_STATE);
            break;
    }
    Serial.println("state: "); Serial.print(device->getState());
}

void prerequisiteIfViewChanged(void) {
    temporaryCollectedImpulses = 0;
    switch (device->getState()) {
        case (INITIALIZE_CHANNEL_A):
        case (INITIALIZE_CHANNEL_B):
            diagView->DrawForm();
            break;
    }
}

void storeCollected() {
    switch (device->getState()) {
        case (INITIALIZE_CHANNEL_A):
            temporaryCollectedImpulses += collector[CHANNEL_A];
            break;
        case (INITIALIZE_CHANNEL_B):
            temporaryCollectedImpulses += collector[CHANNEL_B];
            break;
    }
    for (int index = CHANNEL_A; index <= CHANNEL_B; index++) {
        int val = collector[index];
        collector[index] = 0;
        device->push(index, val);
    }
}

void setup() {
    Serial.begin(115200);

    //Wire.setClock(400000L); this is impossible obn DS1307!
    Wire.begin();
    RTC.begin();

    pinMode(9, INPUT);
    pinMode(8, INPUT);

    pinMode(BUTTON_MODE, INPUT_PULLUP);
    pinMode(BUTTON_PLUS, INPUT_PULLUP);
    pinMode(BUTTON_MINUS, INPUT_PULLUP);

    collector[CHANNEL_A] = collector[CHANNEL_B] = 0;

    buttons.AddEventListener(BUTTON_MODE, buttonModePress, buttonModeRelease);
    buttons.AddEventListener(BUTTON_PLUS, buttonPlusPress, buttonPlusRelease);
    buttons.AddEventListener(BUTTON_MINUS, buttonMinusPress, buttonMinusRelease);

    device = new YaoGuai();
    device->changeViewEventHandler = prerequisiteIfViewChanged;

    display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
    display.cp437(true);
    display.clearDisplay();

    setDateTimeView = new SetDateTimeView(&display, &RTC, device);
    diagView = new ChannelDiagView(&display, &RTC, device);
    defaultView = new DefaultView(&display, &RTC, device);

    if (!RTC.isrunning()) {
        device->setState(SET_TIME);
        RTC.adjust(DateTime(2008, 07, 01, 12, 18, 00));
    } else {
        device->setState(INITIALIZE_CHANNEL_A);
    }
    cli();
    attachInterrupt(2, sensorsInterrupt, RISING);
    attachInterrupt(4, keypadInterrupt, CHANGE);
    sei();
}

void loop() {
    buttons.processIt();

    storeCollected();

    switch (device->getState()) {
        case (INITIALIZE_CHANNEL_A):
            diagView->RenderView(CHANNEL_A, temporaryCollectedImpulses);
            break;
        case (INITIALIZE_CHANNEL_B):
            diagView->RenderView(CHANNEL_B, temporaryCollectedImpulses);
            break;
        case (SET_TIME):
            setDateTimeView->RenderView();
            break;
        case (DEFAULT_STATE):
            defaultView->RenderView();
            break;
    }
    display.display();
}

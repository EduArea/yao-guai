//
// Created by stanislav on 24.06.15.
//

#include <Arduino.h>
#include "YaoGuai.h"


YaoGuai::YaoGuai() {
    changeViewEventHandler = nullptr;
    this->initTL(&channel[CHANNEL_A]);
    this->initTL(&channel[CHANNEL_B]);
    quantity[CHANNEL_A] = quantity[CHANNEL_B] = 2;
    sensitivity[CHANNEL_A] = SENSITIVITY; //a few sensors in each channel!
    sensitivity[CHANNEL_B] = SENSITIVITY; //a few sensors in each channel!
}

void YaoGuai::initTL(TimeLine *tl) {
    for (int i = TIMELINELENGTH - 1; i >= 0; i--)
        tl->count[i] = 0;
    tl->head = 0;
    tl->overflow = false;
    tl->lastCheck = millis();
    tl->max = 0;
}

void YaoGuai::pushInTL(TimeLine *tl, uint16_t value) {
    long currentTime = millis();
    long delta = abs(currentTime - tl->lastCheck) / (TIMELINECAPACITY * 1000);
    if (delta > TIMELINELENGTH) {
        //TODO отреагировать на ошибку -- интервал между измерениями был слишком большой! Событие произойдёт как минимум один раз за 51 день при переполнении millis
        initTL(tl);
        tl->count[tl->head] = value; //there is head on the zero now
        tl->lastCheck = currentTime;
        tl->max = value;
        return;
    }

    if (delta == 0) {
        tl->count[tl->head] += value;
        if (tl->max < tl->count[tl->head])
            tl->max = tl->count[tl->head];
        return;
    }
    long newHead = tl->head + delta;
    long hB = newHead < TIMELINELENGTH ? newHead : TIMELINELENGTH;
    for (long i = tl->head + 1; i < hB; i++) {
        tl->count[i] = 0;
    }
    if (newHead >= (TIMELINELENGTH)) {
        newHead -= TIMELINELENGTH;
        tl->overflow = true;
        for (long i = 0; i < newHead; i++) {
            tl->count[i] = 0;
        }
    }
    tl->head = newHead;
    tl->count[tl->head] = value;
    tl->lastCheck = currentTime;
    tl->max = 0;
    for (uint16_t i = 0; i < TIMELINELENGTH; i++)
        if (tl->max < tl->count[i])
            tl->max = tl->count[i];
}

int YaoGuai::lastValuesHead(void) {
  return this->channel[CHANNEL_A].head;
}

unsigned long YaoGuai::getMRpH(int index) {
    unsigned long pre = (double) (this->sensitivity[index] * this->quantity[index] * getDelta(&channel[index])) / 100;
    if (pre == 0)
        return 0;
    unsigned long value = ((getPulsesInLine(index) * 100) / pre) * 100;
    return value;
}


uint16_t YaoGuai::getPulsesInLine(int index) {
    TimeLine *tl = &this->channel[index];
    uint16_t result = 0;
    for (long i = tl->head; i >= 0; i--) {
        result += tl->count[i];
    }
    if (tl->overflow) {
        for (long i = TIMELINELENGTH - 1; i > tl->head; i--) {
            result += tl->count[i];
        }
    }
    return result;
}

uint16_t YaoGuai::getSecondsInLine(int index) {
    TimeLine *tl = &this->channel[index];
    if (tl->overflow)
        return TIMELINELENGTH * TIMELINECAPACITY;
    else
        return (tl->head + 1) * TIMELINECAPACITY;
}

uint16_t YaoGuai::getDelta(TimeLine *tl) {
    long deltaT = (tl->head + 1) * TIMELINECAPACITY;
    if (tl->overflow) {
        deltaT += (TIMELINELENGTH - tl->head - 1) * TIMELINECAPACITY;
    }
    return deltaT;
}

YaoGuaiState YaoGuai::getState() {
    return this->state;
}

void YaoGuai::setState(YaoGuaiState s) {
    this->state = s;
    if (changeViewEventHandler != nullptr)
        changeViewEventHandler();
}

void YaoGuai::push(int index, int value) {
    this->pushInTL(&channel[index], value);
}

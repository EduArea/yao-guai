//
// Created by Stanislav Lakhtin on 12/07/16.
//

#ifndef YAO_GUAI_CHANNELDIAGVIEW_H
#define YAO_GUAI_CHANNELDIAGVIEW_H

#include <Adafruit_SSD1306.h>
#include <Arduino.h>
#include "YaoGuai.h"
#include "DefaultView.h"

class ChannelDiagView: public DefaultView {
public:
    ChannelDiagView(Adafruit_SSD1306 *display, RTC_DS1307 *rt, YaoGuai *device) : DefaultView(display, rt, device){}
    void RenderView(int index, uint16_t collected);
    void DrawForm( void );
};


#endif //YAO_GUAI_CHANNELDIAGVIEW_H

//
// Created by Stanislav Lakhtin on 12/07/16.
//

#include <Arduino.h>
#include "DefaultView.h"


DefaultView::DefaultView(Adafruit_SSD1306 *dis, RTC_DS1307 *rt, YaoGuai *dev) {
    this->display = dis;
    this->device = dev;
    this->rtc = rt;
    this->autoCompleteChannelDiag = true;
    this->lastChangeModeTime = millis();
    this->showDate = true;
    this->minValue = 10000;
    this->maxValue = 10000;
}

int DefaultView::getCountsOfDigits(unsigned long number) {
    int count = (number == 0) ? 1 : 0;
    while (number != 0) {
        count++;
        number /= 10;
    }
    return count;
}

void DefaultView::RenderView() {

    display->clearDisplay();
    display->setTextColor(WHITE);

    unsigned long  mrpha = device->getMRpH(CHANNEL_A);
    unsigned long  mrphb = device->getMRpH(CHANNEL_B);
    unsigned long  mrph = (mrpha + mrphb) / 2;

    if (minValue > mrph)
      minValue = mrph;
    if (maxValue < mrph)
      maxValue = mrph;

    int dataWidth = (getCountsOfDigits(mrph/1000) + 2) * 16 + (4 * 6); //TODO make it precise and separate in standalone func
    int x_pos = 127 - dataWidth;
    this->printLongAsFloat(mrph, x_pos, 16, 1, 2);
    display->setTextSize(1);
    display->print(F(" mR/h"));

    if (mrph > 15000 && mrph < 30000)
        this->print(F("Natural"), 0, 0);
    else if (mrph >= 30000)
        this->print(F("ATTENTION!"), 0, 0);
    else
        this->print(F("Low"), 0, 0);

    if (!rtc->isrunning()) {
        this->print(F("RTC error"), 0, 0);
    } else {
        if (showDate)
            this->printTime(rtc->now(), 78, 0, 1);
        else
            this->printDate(rtc->now(), 78, 0, 1);
        long lT = millis();
        if (labs(lastTimeViewChanged - lT) > 5000) {
            showDate = !showDate;
            lastTimeViewChanged = lT;
        }
    }
    drawScale();
}

void DefaultView::drawScale(void) {
  uint16_t baseLine = 51;
  uint16_t maxHeight = 14;
  uint16_t lOffset = (getCountsOfDigits(minValue/1000) +2 ) *6 +3;
  uint16_t rOffset = (getCountsOfDigits(maxValue/1000) +2 ) *6 +3;

  bool dot = true;
  uint16_t o = (128-rOffset);
  for (uint16_t i=lOffset; i < o; i++) {
      display->drawPixel(i, baseLine, dot);
      dot = !dot;
  }

  display->fillRect(0, 38, lOffset+2, 14, BLACK);
  display->drawRect(0, 38, lOffset+2, 14, WHITE);
  this->printLongAsFloat(minValue, 3, 42, 1);
  display->fillRect(128-rOffset, 38, rOffset, 14, BLACK);
  display->drawRect(128-rOffset, 38, rOffset, 14, WHITE);
  this->printLongAsFloat(maxValue, 130-rOffset, 42, 1);
  this->print("min   <- mR/h ->  max", 0, 56, 1);

}

void DefaultView::print(const char *value, int x_pos, int y_pos, int size, int color) {
    setTextParam(x_pos, y_pos, size, color);
    this->display->print(value);
}

void DefaultView::printLongAsFloat(unsigned long value, int x_pos, int y_pos, unsigned int positions, int size, int color) {
    setTextParam(x_pos, y_pos, size, color);
    this->display->print(value / 1000);
    long pre = value % 1000;
    if (!positions || positions >3 || positions < 1)
        return;
    else
        this->display->print(F("."));
    char buf[sizeof(char)*4+1];
    char *format = "%03d";
    Serial.println(format);
    sprintf(buf, format, pre);
    buf[positions] = '\0';
    display->print(buf);
}

void DefaultView::print(int value, int x_pos, int y_pos, int size, int color) {
    setTextParam(x_pos, y_pos, size, color);
    this->display->print(value);
}

void DefaultView::print(long value, int x_pos, int y_pos, int size, int color) {
    setTextParam(x_pos, y_pos, size, color);
    this->display->print(value);
}

void DefaultView::print(uint16_t value, int x_pos, int y_pos, int size, int color) {
    setTextParam(x_pos, y_pos, size, color);
    this->display->print(value);
}

void DefaultView::print(DateTime value, int x_pos, int y_pos, int size, int color) {
    setTextParam(x_pos, y_pos, size, color);
    display->print(value.year());
    display->print("/");
    display->print(value.month());
    display->print("/");
    display->print(value.day());
    display->print(" ");
    display->print(value.hour());
    display->print(":");
    display->print(value.minute());
}

void DefaultView::printTime(DateTime value, int x_pos, int y_pos, int size, int color) {
    setTextParam(x_pos, y_pos, size, color);
    display->print(value.hour());
    display->print(":");
    display->print(value.minute());
    display->print(":");
    display->print(value.second());
}

void DefaultView::printDate(DateTime value, int x_pos, int y_pos, int size, int color) {
    setTextParam(x_pos, y_pos, size, color);
    display->print(value.year());
    display->print("/");
    display->print(value.month());
    display->print("/");
    display->print(value.day());
}

void DefaultView::setTextParam(int x_pos, int y_pos, int size, int color) {
    display->setCursor(x_pos, y_pos);
    display->setTextColor(color);
    display->setTextSize(size);
}

void DefaultView::print(const __FlashStringHelper * val, int x_pos, int y_pos, int size, int color) {
    setTextParam(x_pos, y_pos, size, color);
    display->print(val);
}

char *DefaultView::utf8rus(char *source) {
    int i, j, k;
    unsigned char n;
    char m[2] = {'0', '\0'};

    strcpy(bufferUtf8, "");
    k = strlen(source);
    i = j = 0;

    while (i < k) {
        n = source[i];
        i++;

        if (n >= 0xC0) {
            switch (n) {
                case 0xD0: {
                    n = source[i];
                    i++;
                    if (n == 0x81) {
                        n = 0xA8;
                        break;
                    }
                    if (n >= 0x90 && n <= 0xBF) n = n + 0x30;
                    break;
                }
                case 0xD1: {
                    n = source[i];
                    i++;
                    if (n == 0x91) {
                        n = 0xB8;
                        break;
                    }
                    if (n >= 0x80 && n <= 0x8F) n = n + 0x70;
                    break;
                }
            }
        }

        m[0] = n;
        strcat(bufferUtf8, m);
        j++;
        if (j >= MAXSTRINGLENGTH) break;
    }
    return bufferUtf8;
}

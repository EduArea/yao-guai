//
// Created by Stanislav Lakhtin on 12/07/16.
//

#ifndef YAO_GUAI_DEFAULTVIEW_H
#define YAO_GUAI_DEFAULTVIEW_H

#define MAXSTRINGLENGTH 21

#include <Adafruit_SSD1306.h>
#include <RTClib.h>
#include "YaoGuai.h"

class DefaultView {
protected:
    Adafruit_SSD1306 *display;
    YaoGuai *device;
    RTC_DS1307 *rtc;
    long lastChangeModeTime;
    bool autoCompleteChannelDiag;
    int  getCountsOfDigits(unsigned long number);
    void setTextParam(int x_pos, int y_pos, int size, int color);
    bool showDate;
    long lastTimeViewChanged;
    char bufferUtf8[MAXSTRINGLENGTH +1];
    void drawScale(void);
    unsigned long minValue;
    unsigned long maxValue;
public:
    DefaultView(Adafruit_SSD1306 *display, RTC_DS1307 *rt, YaoGuai *device);
    void RenderView();
    void print(const char[], int x_pos, int y_pos, int size=1, int color=WHITE);
    void print(long value, int x_pos, int y_pos, int size=1, int color=WHITE);
    void printLongAsFloat(unsigned long value, int x_pos, int y_pos, unsigned int positions = 1, int size=1, int color=WHITE);
    void print(uint16_t value, int x_pos, int y_pos, int size=1, int color=WHITE);
    void print(int value, int x_pos, int y_pos, int size=1, int color=WHITE);
    void print(DateTime value, int x_pos, int y_pos, int size=1, int color=WHITE);
    void printTime(DateTime value, int x_pos, int y_pos, int size=1, int color=WHITE);
    void printDate(DateTime value, int x_pos, int y_pos, int size=1, int color=WHITE);
    void print(const __FlashStringHelper *val, int x_pos, int y_pos, int size=1, int color=WHITE);
    char* utf8rus(char* source);
};


#endif //YAO_GUAI_DEFAULTVIEW_H
